<?php
class Comment extends AppModel {
	public $validate = array(
        'comment' => array(
            'rule' => 'notBlank'
            )
    );


    public function isOwnedBy($comment, $user) {
    		return $this->field('id', array('id' => $comment, 'uid' => $user)) !== false;
	}  

   
}
