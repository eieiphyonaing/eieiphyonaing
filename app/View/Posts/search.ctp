<p> Your search result is</p>
<table>

    <tr>
        <th>Id</th>
        <th>Title</th>
        <th>Actions</th>
        <th>Created</th>
    </tr>
    <?php foreach ($results as $result): ?>
    <tr>
        <td><?php echo $result['Post']['id']; ?></td>
        <td>
            <?php
                echo $this->Html->link(
                    $result['Post']['title'],
                    array('action' => 'view', $result['Post']['id'])
                );
            ?>
        </td>
        <td>
            <?php
                echo $this->Form->postLink(
                    'Delete',
                    array('action' => 'delete', $result['Post']['id']),
                    array('confirm' => 'Are you sure?')
                );
            ?>
            <?php
                echo $this->Html->link(
                    'Edit', array('action' => 'edit', $result['Post']['id'])
                );
            ?>
        </td>
        <td>
            <?php echo $result['Post']['created']; ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
