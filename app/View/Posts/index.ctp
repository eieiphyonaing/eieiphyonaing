<h1>Blog posts</h1>
<p > <?php echo $this->Html->link('Add Post', array('action' => 'add')); ?>

<?php
    echo $this->Html->link(
 'Logout', array('controller'=>'users','action' => 'logout'));?></P>
<form   action = "posts/search" method="post">
<table>
<tr>
<th>Id</th>
<th>Title</th>
</tr>
<tr>
<th><input type = "text" name = "id"></th>
<th><input type = "text" name = "Title"></th>
</tr>
</table>
<input type= "submit" value = "Search" style="background-color:green;">
</form> 
<table>
    <tr>
        <th>Id</th>
        <th>Title</th>
        <th>Actions</th>
        <th>Created</th>
    </tr>
    <?php foreach ($posts as $post): ?>
    <tr>
        <td><?php echo $post['Post']['id']; ?></td>
        <td>
            <?php
                echo $this->Html->link(
                    $post['Post']['title'],
                    array('action' => 'view', $post['Post']['id'])
                );
            ?>
        </td>
        <td>
            <?php
                echo $this->Form->postLink(
                    'Delete',
                    array('action' => 'delete', $post['Post']['id']),
                    array('confirm' => 'Are you sure?')
                );
            ?>
            <?php
                echo $this->Html->link(
                    'Edit', array('action' => 'edit', $post['Post']['id'])
                );
            ?>
        </td>
        <td>
            <?php echo $post['Post']['created']; ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>
<?php unset($post); ?>
    <?php echo $this->Paginator->prev('<<'.__('previous', true), array(), null, array('class' => 'disabled')); ?>
    <?php echo $this->Paginator->numbers(); ?>
    <?php echo $this->Paginator->next(__('next',true).' >>', array(), null, array('class' => 'disabled')); ?> 

