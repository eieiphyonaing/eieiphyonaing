<h1><?php  echo h($post['Post']['title']); ?></h1>

<p><small>Created: <?php echo $post['Post']['created']; ?></small></p>

<p><?php echo h($post['Post']['body']); ?></p>
<p><?php echo $this->Html->image($post['Post']['image_url'],array('width' => '100px','height' => '100px')); ?> </p>



<h1>Add Comment</h1>
<?php
echo $this->Form->create('Comment',array('enctype'=>'multipart/form-data','url' =>
	                                              array('controller' => 'posts', 'action' => 'comment')));
echo $this->Form->input('comment', array('rows' => '3'));
echo $this->Form->input('pid', array('type' => 'hidden', 'value'=> $post['Post']['id'])); 
echo $this->Form->input('Image', array('type' =>'file'));
 
echo $this->Form->end('Add comment'); ?>
 
<table>
    <tr>
        <th>Id</th>
        <th>Title</th>
        <th>Actions</th>
        <th>Username</th>
        
    </tr>
    <?php foreach ($comments as $comment): ?>
    <tr>
        <td><?php echo $comment['Comment']['id']; ?></td>
        <td>
            <?php
                echo $comment['Comment']['comment'];
                
            ?>
            <?php echo $this->Html->image($comment['Comment']['image_url'],array('width' => '100px','height' => '100px')); ?> 

        </td>

        <td>
            <?php
                echo $this->Form->postLink(
                    'Delete',
                    array('action' => 'delete_comment', $comment['Comment']['id'],$comment['Comment']['pid']),
                    array('confirm' => 'Are you sure?')
                );
            ?>
            <?php
                echo $this->Html->link(
                    'Edit', array('action' => 'edit_comment', $comment['Comment']['id'],$comment['Comment']['pid']));
                            ?>
        </td>
         <td><?php echo $comment['Comment']['username'];?></td>
        
    </tr>
    <?php endforeach; ?>

</table>
