<?php
App::uses('AppController', 'Controller');
class PostsController extends AppController {
    public $helpers = array('Html', 'Form');
    public $components=array('Paginator');
    var $uses = array('Post','Comment');

    public function index() {
    $this->Paginator->settings=array('limit' => 5);
       $data=$this->Paginator->paginate();
       $this->set('posts',$data);
    }
     public function view($id = null)
    {
        $post = $this->Post->findById($id);
        $comments = $this->Comment->find('all', array('conditions' => array('Comment.pid LIKE' => "%".$id."%")));
        $this->set('post',$post);
        $this->set('comments',$comments);
    }
    public function add() {
        if ($this->request->is('post')) {
            $this->Post->create();
            $this->request->data['Post']['uid'] = $this->Auth->user('id');
            if(!empty($this->data)){
                    if(!empty($this->request->data['Post']['Image']['name'])){
                        $file = $this->request->data['Post']['Image']; 
                        $ext = substr(strtolower(strrchr($file['name'], '.')), 1); 
                        $arr_ext = array('jpg', 'jpeg', 'gif','png'); 
                        if(in_array($ext, $arr_ext)){
                            move_uploaded_file($file['tmp_name'], WWW_ROOT . '/img/' . $file['name']);
                            $this->request->data['Post']['image_url'] = '/img/'. $file['name'];
                        }
                    }       
            }
            if ($this->Post->save($this->request->data)) {
                $this->Flash->success(__('Your post has been saved.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to add your post.'));
        }
    }


    public function edit($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }
        $post = $this->Post->findById($id);
        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->Post->id = $id;
            if (!empty($this->data)){
                if (!empty($this->request->data['Post']['Image']['name'])) {
                        $file = $this->request->data['Post']['Image']; 
                        $ext = substr(strtolower(strrchr($file['name'], '.')), 1); 
                        $arr_ext = array('jpg', 'jpeg', 'gif','png'); 
                        if (in_array($ext, $arr_ext)){
                            move_uploaded_file($file['tmp_name'], WWW_ROOT . '/img/' . $file['name']);
                            $this->request->data['Post']['image_url'] = '/img/'. $file['name'];
                        }
                }

            }
            if ($this->Post->save($this->request->data)) {
                $this->Flash->success(__('Your post has been updated.'));if (condition) {
                }
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to update your post.'));
        }

        if (!$this->request->data) {
            $this->request->data = $post;
        }   
    }


    public function delete($id) {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }

        if ($this->Post->delete($id)) {
            $this->Flash->success(
                __('The post with id: %s has been deleted.', h($id))
            );
        } 
        else {
            $this->Flash->error(
                __('The post with id: %s could not be deleted.', h($id))
            );
        }

        return $this->redirect(array('action' => 'index'));
    }
     
    public function search() {
        $id = $this->request->data['id'];
        $title = $this->request->data['Title'];
        if ($id == "" && $title == "") {
            $this->Flash->error(__('Please fill you want to search'));
            return $this->redirect(array('action' => 'index'));
        }
        else if ($id != "" && $title == "") {
              $results = $this->Post->find('all', array(
                'conditions' => array('Post.id LIKE' => "%". $id ."%")));
        }
        else if ($id == "" && $title != "") {
                $results = $this->Post->find('all', array(
                    'conditions' => array('Post.title LIKE' => "%". $title ."%")));
        }
        else{
            $this->Flash->error(__('Please fill only one you want to search'));
              return $this->redirect(array('action' => 'index'));
        }
        $this->set('results',$results);
    }


    public function comment() {   
        if ($this->request->is('post')) {
            $this->Comment->create();
            $this->request->data['Comment']['uid'] = $this->Auth->user('id');
            $this->request->data['Comment']['username'] = $this->Auth->user('username');

            if (!empty($this->data)){
                if (!empty($this->request->data['Comment']['Image']['name'])){
                    $file = $this->request->data['Comment']['Image'];
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1); 
                    $arr_ext = array('jpg', 'jpeg', 'gif','png'); 
                    if (in_array($ext, $arr_ext)){
                        move_uploaded_file($file['tmp_name'], WWW_ROOT . '/img/' . $file['name']);
                        $this->request->data['Comment']['image_url'] = '/img/'. $file['name'];
                    }
                }  
            }
            if ($this->Comment->save($this->request->data)) {
            $this->Flash->success('Your comment has been added.');
            return $this->redirect(array('controller' => 'posts', 'action' => 'view/'.$this->request->data['Comment']['pid']));
            } 
            else {
            $this->Flash->success('Unable to add your comment.');
            }
        }   
    }

    public function isAuthorized($user) {
        if ($this->action === 'add') {
            return true;
        }
        if (in_array($this->action, array('edit', 'delete'))) {
            $postId = (int) $this->request->params['pass'][0];
            if ($this->Post->isOwnedBy($postId, $user['id'])) {
                return true;
            }
        }

        if (in_array($this->action, array('edit_comment', 'delete_comment'))) {
            $commentId = (int) $this->request->params['pass'][0];
            if ($this->Comment->isOwnedBy($commentId, $user['id'])) {
                return true;
            }
        }

        return parent::isAuthorized($user);
    }

    public function edit_comment($id = null, $pid = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid comment'));
        }

        $comment = $this->Comment->findById($id);
        if (!$comment) {
            throw new NotFoundException(__('Invalid comment'));
        }

        if ($this->request->is(array('post', 'put'))) {
            $this->Comment->id = $id;
            $this->Comment->pid = $pid;
            if (!empty($this->data)){
                    if (!empty($this->request->data['Comment']['Image']['name'])){
                        $file = $this->request->data['Comment']['Image']; 
                        $ext = substr(strtolower(strrchr($file['name'], '.')), 1); 
                        $arr_ext = array('jpg', 'jpeg', 'gif','png'); 
                        if (in_array($ext, $arr_ext)){
                            move_uploaded_file($file['tmp_name'], WWW_ROOT . '/img/' . $file['name']);
                            $this->request->data['Comment']['image_url'] = '/img/'. $file['name'];
                        }
                    }
            }
            if ($this->Comment->save($this->request->data)) {
                $this->Flash->success(__('Your comment has been updated.')) ;
                //return $this->redirect(array('controller' => 'posts', 'action' => 'view', $this->request->data['Comment']['pid']));
                //return $this->redirect(array('controller' => 'posts', 'action' => 'view/'.$this->request->data['Comment']['pid']));
                //return $this->redirect(array('controller' => 'posts', 'action' => 'view/'.$this->request->data['Comment']['pid']));
                return $this->redirect(array('action' => 'index'));
            }
            else {
                $this->Flash->error(__('Unable to update comment.'));
            }
        }

        if (!$this->request->data) {
            $this->request->data = $comment;
        }   
    }


    public function delete_comment($id = null, $pid = null) {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        if ($this->Comment->delete($id)) {
     http://cakephp-upload.readthedocs.org/en/latest/examples.html#basic-example       $this->Flash->success(
                __('The comment with id: %s has been deleted.', h($id));
        }
     else {
            $this->Flash->error(
                __('The comment with id: %s could not be deleted.', h($id))
            );
        }

       return $this->redirect(array('action' => 'index'));

    }



}






                
            