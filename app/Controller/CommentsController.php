<?php
App::uses('AppController', 'Controller');
class CommentsController extends AppController {
	public function comment() {
    	if ($this->request->is('post')) {
        $this->Comment->create();
         $this->request->data['Comment']['uid'] = $this->Auth->user('id');
        	if ($this->Comment->save($this->request->data)) {
            	$this->Flash->success('Your comment has been added.');
            	//$this->redirect(array('action' => 'index'));
            	return $this->redirect(array('controller' => 'posts', 'action' => 'view'));
       		 } 
       		else {
            $this->Flash->success('Unable to add your comment.');
       		 }
    	}
	}
	public function view($id = null)
    {
        $this->Post->id = $id;
        $this->set('post',$this->Post->read());
    }

  /*  public function search() {
       $id = $this->request->data['id'];
        $title = $this->request->data['Title'];
        if ($id == "" && $title == "") {
            $this->Flash->error(__('Please fill you want to search'));
            return $this->redirect(array('action' => 'index'));
            //pr($this->request->data);
         }
         else if ($id != "" && $title == "") {
              $results = $this->Post->find('all', array(
                'conditions' => array('Post.id' == $id )));
         }
         else if ($id == "" && $title != "") {
                $results = $this->Post->find('all', array(
                    'conditions' => array('Post.title' == $title ."%")));
         }
         else{
            $this->Flash->error(__('Please fill only one you want to search'));
              return $this->redirect(array('action' => 'index'));
         }
         $this->set('results',$results);
    }*/


    
}